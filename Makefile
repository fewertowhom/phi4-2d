all: run

run: main.cpp
	g++ -std=c++17 -O2 -march=native main.cpp -o run

clean:
	rm -rf run

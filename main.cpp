#include <vector>
#include <random>
#include <fstream>
#include <iostream>
#include <algorithm>

int constexpr SEED = 42;

struct Field {
	int const Nt, Nx;
	double const kappa;
	double const lambda;
	std::vector<double> field;
	std::vector<int> nextT, nextX, prevT, prevX;

	// random number generation
	std::mt19937 gen;
	std::uniform_real_distribution<> ud;
	std::normal_distribution<> nd;


	int index(int const x, int const y) const { return x * Nx + y; }

	Field(double const Kappa, double const Lambda, int const nx, int const ny)
	: Nt{nx}
	, Nx{ny}
	, kappa{Kappa}
	, lambda{Lambda}
	, field{std::vector<double>(Nt*Nx)}
	, nextT{std::vector<int>(Nt)}
	, nextX{std::vector<int>(Nx)}
	, prevT{std::vector<int>(Nt)}
	, prevX{std::vector<int>(Nx)}
	, gen{SEED}
	, ud{0.0, 1.0}
	, nd{0.0, 1.0}
	{
		// random initial configuration;
		for (int i = 0; i < Nt; ++i)
			for (int j = 0; j < Nx; ++j)
				field[index(i, j)] = nd(gen);

		for (int i = 0; i < Nt; ++i) {
			nextT[i] = i + 1;
			prevT[i] = i - 1;
		}

		for (int i = 0; i < Nx; ++i) {
			nextX[i] = i + 1;
			prevX[i] = i - 1;
		}

		// periodic boundary conditions
		nextT[Nt-1] = 0;
		prevT[0]    = Nt-1;
		nextX[Nx-1] = 0;
		prevX[0]    = Nx-1;
	}

	double Action() const {
		double action = 0.0;
		for (int i = 0; i < Nt; ++i) {
			for (int j = 0; j < Nx; ++j) {
				// potential term
				auto const f2 = field[index(i, j)] * field[index(i, j)];
				auto const f4 = f2 * f2;

				// kinetic term in X direction
				auto const kinT = field[index(i, j)] *
					(field[index(nextT[i], j)]);// + field[index(prevT[i], j)]);
				auto const kinX = field[index(i, j)] *
					(field[index(i, nextX[j])]);// + field[index(i, prevX[j])]);

				action += (1.0 - 2.0 * lambda) * f2 + lambda * f4
					- 2.0 * kappa * (kinT + kinX);
			}
		}
		return action;
	}

	double MetropolisStep(double const sigma) {
		int rejected = 0;
		
		// sweep over the lattice
		for (int i = 0; i < Nt; ++i) {
			for (int j = 0; j < Nx; ++j) {
				int const idx = index(i, j);

				auto const oldAction = Action();
				auto const oldFieldValue = field[idx];

				// propose update
				field[idx] += nd(gen) * sigma;
				auto const dS = Action() - oldAction;
				if (dS > 0) {						// action has increased
					auto const r = ud(gen);
					if (r >= std::exp(-dS))	{		// reject update
						field[idx] = oldFieldValue;
						rejected++;
					}
				}
			}
		}
		// if the action has decreased, or if r < exp(-S)
		// the field retains the proposed value

		return 1.0 - (1.0 *rejected) / (Nt*Nx); // ratio of accepted proposals in one sweep
	}

	double Average() const {
		return std::sqrt(2.0 * kappa) * 
			std::accumulate(field.cbegin(), field.cend(), 0.0) / (Nt * Nx); }

	double operator[](int const i) const { return field[i]; }
};

double StDev(std::vector<double> const& vec) {
	auto const mean = std::accumulate(vec.cbegin(), vec.cend(), 0.0) / vec.size();
	double sum = 0.0;
	for (auto const e : vec)
		sum += (e - mean) * (e - mean);
	sum /= vec.size() - 1;
	return std::sqrt(sum);
}

int main() {
	// output file name
	std::string const fileName = "0.272.dat";

	// lattice dimensions
	int constexpr Nt = 32;
	int constexpr Nx = 32;

	double constexpr lambda = 0.02;
	double constexpr kappa  = 0.272;

	// MC parameters
	int constexpr nTherm = 1000;
	int constexpr nSteps = 100000;
	int constexpr nSkip  = 100;

	auto phi = Field{kappa, lambda, Nt, Nx};

	auto mag = std::vector<double>{};
	mag.resize(nSteps / nSkip);

	// thermalise system
	for (int i = 0; i < nTherm; ++i)
		phi.MetropolisStep(1.0);

	std::cout << "# kappa  = " << kappa << '\n'
			  << "# lambda = " << lambda << '\n';

	double counter = 0.0;
	int j = 0;
	for (int i = 0; i < nSteps; ++i) {
		counter += phi.MetropolisStep(1.0);
		if (i % nSkip == 0) {
			auto const tmp = phi.Average();
			std::cout << i << '\t' << tmp << '\n';
			mag[j] = tmp;
			j++;
		}
	}

	// average magnetisation
	auto const AVG = std::accumulate(mag.cbegin(), mag.cend(), 0.0) / mag.size();

	auto sus = std::vector<double>{};
	sus.resize(nSteps / nSkip);
	std::transform(mag.cbegin(), mag.cend(), sus.begin(),
			[AVG](double const mag) { return (mag - AVG)*(mag - AVG); });

	// average susceptibility
	auto const SUS = std::accumulate(sus.cbegin(), sus.cend(), 0.0) / sus.size();

	std::cout << "# acceptance rate: " << counter / nSteps << '\n'
			  << "# average field:   " << AVG
			  << " +/- " << StDev(mag) / std::sqrt(mag.size()) << '\n'
			  << "# susceptibility:  " << Nt * Nx * SUS
			  << " +/- " << Nt * Nx * StDev(sus) / std::sqrt(sus.size()) << '\n';

	// output to file magnetisation and susceptibility for each MC step
	auto fi = std::ofstream(fileName);
	fi << "# kappa  = " << kappa << '\n'
	   << "# lambda = " << lambda << '\n';
	for (int i = 0; i < sus.size(); ++i)
		fi << i << '\t' << mag[i] << '\t' << sus[i] << '\n';

	fi << "# acceptance rate: " << counter / nSteps << '\n'
	   << "# average field:   " << AVG
	   << " +/- " << StDev(mag) / std::sqrt(mag.size()) << '\n'
	   << "# susceptibility:  " << Nt * Nx * SUS
	   << " +/- " << Nt * Nx * StDev(sus) / std::sqrt(sus.size()) << '\n';

	fi.close();

	return 0;
}

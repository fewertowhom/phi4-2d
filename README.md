# Phi4-2d

C++ implementation of the Metropolis algorithm to simulate a phi^4 field in two dimensions.
This is a very naïve implementation, not at all optimised. But it is good for learning purposes.
